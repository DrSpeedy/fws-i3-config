NAME=fws-i3-config
VERSION=0.1a

BLD_DIR=/tmp/$(NAME)
PKG_NAME=$(NAME)-$(VERSION)
PKG=$(PKG_NAME).tar.gz
SIG=$(PKG).asc

SYSCONF=/etc
PREFIX?=/usr
DOC_DIR=$(PREFIX)/share/doc/$(PKG_NAME)

pkg:
	tar -czf $(PKG) *

build:
	mkdir -p $(BLD_DIR)/$(SYSCONF)
	mkdir -p $(BLD_DIR)/$(PREFIX)/share/$(NAME)
	cp -r * $(BLD_DIR)/$(PREFIX)/share/$(NAME)
	mv $(BLD_DIR)/$(PREFIX)/share/$(NAME)/scripts/bin $(BLD_DIR)/$(PREFIX)/bin
	mv $(BLD_DIR)/$(PREFIX)/share/$(NAME)/scripts/profile $(BLD_DIR)/$(SYSCONF)/profile.d
	rm -f $(BLD_DIR)/$(PREFIX)/share/$(NAME)/Makefile
	rm -f $(BLD_DIR)/$(PREFIX)/share/$(NAME)/$(PKG)
	rm -f $(BLD_DIR)/$(PREFIX)/share/$(NAME)/$(SIG)

$(SIG): pkg
	gpg --sign --detach-sign --armor $(PKG)

sign: $(SIG)

clean:
	rm -rf $(PKG) $(SIG) $(BLD_DIR)

all: pkg $(SIG)

tag:
	git tag $(VERSION)
	git push --tags

release: pkg $(SIG) tag

install: build
	mkdir -p $(SYSCONF)/profile.d
	mkdir -p $(PREFIX)/share/$(NAME)
	mkdir -p $(PREFIX)/bin
	cp -r $(BLD_DIR)/* /

uninstall:
	rm -rf $(PREFIX)/share/$(NAME)
	rm -f $(PREFIX)/bin/{i3-gen-config,i3-gen-theme,i3-launch}
	rm -f $(SYSCONF)/fws-env.sh

.PHONY: build sign clean test tag release install uninstall all
