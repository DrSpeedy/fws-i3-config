#!/bin/sh

if [[ ! $XDG_CONFIG_HOME ]]; then
    export XDG_CONFIG_HOME="${HOME}/.config"
fi

if [[ ! $XDG_CACHE_HOME ]]; then
    export XDG_CACHE_HOME="${HOME}/.cache"
fi

session="$(env | egrep DESKTOP_SESSION | awk -F'=' '{print $2}' | awk -F'/' '{print $NF}')"

if [[ $session -eq "i3" ]]; then

    if [[ ! -d ${XDG_CONFIG_HOME}/i3 ]]; then
        mkdir -p ${XDG_CONFIG_HOME}/i3
        cp -r /usr/share/fws-i3-config/* ${XDG_CONFIG_HOME}/i3
    fi

    # Regenerate i3 user's configuration on login
    i3-gen-conf
fi
